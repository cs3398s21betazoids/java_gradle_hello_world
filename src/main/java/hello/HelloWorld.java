package hello;
/* Get a time library */
import org.joda.time.LocalTime;

// Add a comment
// add the comment again
// Dylan Ray assignment 11 comment
// Brett Owen's far superior assignment 11 comment
// Michael Kammer Assignment 11 commment 

public class HelloWorld 
{
	/* master comment */
  public static void main(String[] args) {
    LocalTime currentTime = new LocalTime();
    System.out.println("The current local time is: " + currentTime);
    
    Greeter greeter = new Greeter();
    System.out.println(greeter.sayHello());
  }
}

