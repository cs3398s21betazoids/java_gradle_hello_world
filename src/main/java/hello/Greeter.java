package hello;

// Dylan Ray assignment 11 comment
// Michael Kammer's better Comment for Assignment 11

public class Greeter {

  private String name = "";
  

  public String getName() 

  {
    return name;
  }


  public void setName(String name) 

  {
      this.name = name;
  }


  public String sayHello() 

  {
  	if (name == "") 

    {
       return "Hello!";
    }
    else 
    {
       return "Hello " + name + "!";
    }

  }

    // returns true if this.name is not empty
    public boolean hasName()
    {
	if (name == "")
	    {
		return false;
	    }
	else
	    {
		return true;
	    }
    }

    public boolean isNotMichael()
    {
        if (name != "Michael")
        {
            return true;
        }
        else 
        {
            return false;
        }
    }
}
