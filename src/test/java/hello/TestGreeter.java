package hello;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

// Change the comment again to kick off a build
// This change is made originally on the ted2branch
// Dylan Ray assignment 11 comment
// Brett Owen's far superior assignment 11 comment
// Michael Kammer assignment 11 comment Def far more superior comment.

public class TestGreeter {

   private Greeter g = new Greeter();

   @Test
   @DisplayName("Test for Empty Name")
   public void testGreeterEmpty() 
   {
      assertEquals(g.getName(),"");
      assertEquals(g.sayHello(),"Hello!");
   }

   @Test
   @DisplayName("Test for Name='World'")
   public void testGreeter() 
   {
      g.setName("World");
      assertEquals(g.getName(),"World");
      assertEquals(g.sayHello(),"Hello World!");
   }

    @Test
    @DisplayName("Test for Name='Brett'")
    public void testGreeterBrett()
    {
	g.setName("Brett");
	assertEquals(g.getName(),"Brett");
	assertEquals(g.sayHello(),"Hello Brett!");
    }

    @Test
    @DisplayName("Test for Name='Dylan'")
    public void testGreeterYourName()
    {
	g.setName("Dylan");
	assertEquals(g.getName(),"Dylan");
	assertEquals(g.sayHello(),"Hello Dylan!");
    }

    @Test
    @DisplayName("Test with Empty Name, thus hasName() returns false")
    public void hasNameTest()
    {
	g.setName("");
	assertFalse(g.hasName());
    }

    @Test
    @DisplayName("Test with an Endline for the name")
    public void endlineTest()
    {
	g.setName("/n");
	assertFalse(!g.hasName());
    }

    @Test
    @DisplayName("Test with Character, And because a Character, returns true.")
    public void myNameTest()
    {
        g.setName("g");
        assertFalse(!g.isNotMichael());
    }

    @Test
    @DisplayName("Test for Name='Michael'")
    public void testMyName()
    {
        g.setName("Michael");
        assertEquals(g.getName(),"Michael");
        assertEquals(g.sayHello(),"Hello Michael!");
    }

}
